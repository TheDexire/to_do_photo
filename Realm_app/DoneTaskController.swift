//
//  DoneTaskController.swift
//  Realm_app
//
//  Created by user162217 on 10/9/20.
//  Copyright © 2020 user162217. All rights reserved.
//

import UIKit
import RealmSwift

class DoneTaskController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    
    let realm = try! Realm()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.tableFooterView = UIView()
        NotificationCenter.default.addObserver(self, selector: #selector(self.reloadView), name: NSNotification.Name(rawValue: "reloadTask"), object: nil)
    }
    
    @objc func reloadView(){
        tableView.reloadData()
    }
    
    private func handleMoveToTrash(task_id : Int) {
        
        try! realm.write {
            let data = self.realm.objects(Task.self).filter("id == \(task_id)").first
            realm.delete(data!)
        }
        self.tableView.reloadData()
    }
    
    private func handleDone(task_id : Int) {
        let task = self.realm.objects(Task.self).filter("id == \(task_id)").first
        
        try! self.realm.write {
            task!.status = 0
        }
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadTask"), object: nil)
        
        self.tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView,
                   trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        // Trash action
        let trash = UIContextualAction(style: .destructive,
                                       title: "Удалить") { [weak self] (action, view, completionHandler) in
                                        self?.handleMoveToTrash(task_id: self!.realm.objects(Task.self)[indexPath.row].id)
                                        completionHandler(true)
        }
        trash.image = #imageLiteral(resourceName: "trash")
        
        let configuration = UISwipeActionsConfiguration(actions: [trash])
        
        return configuration
    }
    
    func tableView(_ tableView: UITableView,
                   leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        // Undo action
        let undo = UIContextualAction(style: .normal,
                                      title: "Вернуть") { [weak self] (action, view, completionHandler) in
                                        self?.handleDone(task_id: self!.realm.objects(Task.self)[indexPath.row].id)
                                        completionHandler(true)
        }
        undo.image = #imageLiteral(resourceName: "undo")
        undo.backgroundColor = .systemOrange
        
        let configuration = UISwipeActionsConfiguration(actions: [undo])
        
        return configuration
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return realm.objects(Task.self).filter("status == 1").count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TableViewCell
        
        cell.task_name.text = realm.objects(Task.self).filter("status == 1")[indexPath.row].name
        
        return cell
    }
    
}
