//
//  ViewController.swift
//  Realm_app
//
//  Created by user162217 on 9/30/20.
//  Copyright © 2020 user162217. All rights reserved.
//

import UIKit
import RealmSwift


class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    
    let realm = try! Realm()
    
    let tasks = Task()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.tableFooterView = UIView()
        NotificationCenter.default.addObserver(self, selector: #selector(self.reloadView), name: NSNotification.Name(rawValue: "reloadTask"), object: nil)
    }
    
    @objc func reloadView(){
        tableView.reloadData()
    }
    
    @IBAction func addTapped(_ sender: UIBarButtonItem) {
        //1. Create the alert controller.
        let alert = UIAlertController(title: "Добавить задачу", message: "Введите описание задачи", preferredStyle: .alert)
        
        //2. Add the text field. You can configure it however you need.
        alert.addTextField { (textField) in
            textField.placeholder = "Например: Фото на фоне заката"
        }
        alert.addAction(UIAlertAction(title: "Отмена", style: .cancel, handler: nil ))
            
        alert.addAction(UIAlertAction(title: "Сохранить", style: .default, handler: { [weak alert] (_) in
            let textField = alert?.textFields![0] // Force unwrapping because we know it exists.
            if textField?.text != ""{
            
            try! self.realm.write {
                let task = Task()
                task.id = IncrementaID_task()
                task.name = textField?.text ?? ""
                
                self.realm.add(task)
            }
                
            
            self.tableView.reloadData()
            }
            
        }))
        
        // 4. Present the alert.
        self.present(alert, animated: true, completion: nil)
    }
    
    private func handleMoveToTrash(task_id : Int) {
        
        try! realm.write {
            let data = self.realm.objects(Task.self).filter("id == \(task_id)").first
            realm.delete(data!)
        }
        self.tableView.reloadData()
    }
    
    private func handleEdit(task_id : Int) {
        
        let task = self.realm.objects(Task.self).filter("id == \(task_id)").first
        
        //1. Create the alert controller.
        let alert = UIAlertController(title: "Добавить задачу", message: "Введите описание задачи", preferredStyle: .alert)
        
        //2. Add the text field. You can configure it however you need.
        alert.addTextField { (textField) in
            textField.placeholder = "Например: Например: Фото на фоне заката"
            textField.text = task?.name
        }
        
        // 3. Grab the value from the text field, and print it when the user clicks OK.
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { [weak alert] (_) in
            let textField = alert?.textFields![0] // Force unwrapping because we know it exists.
            
            try! self.realm.write {
                task!.name = textField?.text ?? ""
            }
            
            self.tableView.reloadData()
            
        }))
        
        // 4. Present the alert.
        self.present(alert, animated: true, completion: nil)
    }
    
    private func handleDone(task_id : Int) {
        let task = self.realm.objects(Task.self).filter("id == \(task_id)").first
        
        try! self.realm.write {
            task!.status = 1
        }
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadTask"), object: nil)
        self.tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView,
                   trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        // Trash action
        let trash = UIContextualAction(style: .destructive,
                                       title: "Удалить") { [weak self] (action, view, completionHandler) in
                                        self?.handleMoveToTrash(task_id: self!.realm.objects(Task.self)[indexPath.row].id)
                                        completionHandler(true)
        }
        trash.image = #imageLiteral(resourceName: "trash")
        // Edit action
        let edit = UIContextualAction(style: .normal,
                                      title: "Редактировать") { [weak self] (action, view, completionHandler) in
                                        self?.handleEdit(task_id: self!.realm.objects(Task.self)[indexPath.row].id)
                                        completionHandler(true)
        }
        edit.image = #imageLiteral(resourceName: "edit")
        edit.backgroundColor = .systemOrange
        
        let configuration = UISwipeActionsConfiguration(actions: [trash, edit])
        
        return configuration
    }
    
    func tableView(_ tableView: UITableView,
    leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        // Done action
        let done = UIContextualAction(style: .normal,
                                      title: "Завершить") { [weak self] (action, view, completionHandler) in
                                        self?.handleDone(task_id: self!.realm.objects(Task.self)[indexPath.row].id)
                                        completionHandler(true)
        }
        done.image = #imageLiteral(resourceName: "done")
        done.backgroundColor = .systemOrange
        
        let configuration = UISwipeActionsConfiguration(actions: [done])
        
        return configuration
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return realm.objects(Task.self).filter("status == 0").count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TableViewCell
        
        cell.task_name.text = realm.objects(Task.self).filter("status == 0")[indexPath.row].name
        
        return cell
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        if let indexPath = self.tableView.indexPathForSelectedRow {
            tableView.deselectRow(at: indexPath, animated: true)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "Detail_Segue" {
            if let indexPath = tableView.indexPathForSelectedRow{
                let detailVC = segue.destination as! DetailController
                detailVC.task = self.realm.objects(Task.self)[indexPath.row]
            }
        }
    }
}

