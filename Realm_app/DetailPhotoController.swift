//
//  DetailPhotoController.swift
//  Realm_app
//
//  Created by user162217 on 10/8/20.
//  Copyright © 2020 user162217. All rights reserved.
//

import UIKit
import RealmSwift

class DetailPhotoController: UIViewController {

    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var desc: UILabel!
    
    var photo = Photo()
    let realm = try! Realm()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        getData()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.reloadView), name: NSNotification.Name(rawValue: "reloadPhotoData"), object: nil)
    }
    
    @objc func reloadView(){
        self.viewDidLoad()
    }
    
    func getData(){
        let photo = self.realm.objects(Photo.self).filter("id == \(self.photo.id)").first
        desc.text = photo?.desc
        img.image = UIImage(data: (photo?.picture!)!)
    }
    
    @IBAction func editTapped(_ sender: UIBarButtonItem) {
        let photo = self.realm.objects(Photo.self).filter("id == \(self.photo.id)").first
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let myAlert = storyboard.instantiateViewController(withIdentifier: "alert") as! AddPhotoAlertController
        myAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        myAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        myAlert.photo = photo!
        myAlert.operation_type = 1
        self.present(myAlert, animated: true, completion: nil)
    }
    
    @IBAction func deleteTapped(_ sender: UIBarButtonItem) {
        let alert = UIAlertController(title: "Внимание", message: "вы действительно хотите удалить этот объект?", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Нет", style: .cancel, handler: nil ))
            
        alert.addAction(UIAlertAction(title: "Да", style: .default, handler: { action in
            
            self.deleteObject()
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadPhotoData"), object: nil)
            
            _ = self.navigationController?.popViewController(animated: true)
            
        }))
        
        // 4. Present the alert.
        self.present(alert, animated: true, completion: nil)
        
    }
    
    func deleteObject(){
        let photo = self.realm.objects(Photo.self).filter("id == \(self.photo.id)").first
        try! self.realm.write {
            self.realm.delete(photo!)
        }
    }
}
