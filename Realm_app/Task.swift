//
//  myDog.swift
//  Realm_app
//
//  Created by user162217 on 9/30/20.
//  Copyright © 2020 user162217. All rights reserved.
//

import Foundation
import RealmSwift

class Task: Object {
    @objc dynamic var id = 0
    @objc dynamic var name = ""
    @objc dynamic var status = 0
    let photos = List<Photo>()
    
    override static func primaryKey() -> String? {
        return "id"
    }
}

class Photo: Object {
    @objc dynamic var id = 0
    @objc dynamic var desc = ""
    @objc dynamic var status = 0
    @objc dynamic var picture: Data? = nil
    
    override static func primaryKey() -> String? {
        return "id"
    }
}



//Incrementa ID
func IncrementaID_task() -> Int{
    let realm = try! Realm()
    return (realm.objects(Task.self).max(ofProperty: "id") as Int? ?? 0) + 1
}

func IncrementaID_photo() -> Int{
    let realm = try! Realm()
    return (realm.objects(Photo.self).max(ofProperty: "id") as Int? ?? 0) + 1
}
