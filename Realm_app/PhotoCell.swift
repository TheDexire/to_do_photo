//
//  PhotoCell.swift
//  Realm_app
//
//  Created by user162217 on 10/1/20.
//  Copyright © 2020 user162217. All rights reserved.
//

import UIKit

class PhotoCell: UICollectionViewCell {
    
    @IBOutlet weak var photo: UIImageView!
    @IBOutlet weak var check_view: UIView!
    @IBOutlet weak var check_view_active: UIView!
    @IBOutlet weak var desc: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        check_view.layer.cornerRadius = check_view.frame.size.width/2
        check_view.clipsToBounds = true
        
        check_view_active.layer.cornerRadius = check_view_active.frame.size.width/2
        check_view_active.clipsToBounds = true
    }
    
}
