//
//  AddPhotoAlertController.swift
//  Realm_app
//
//  Created by user162217 on 10/2/20.
//  Copyright © 2020 user162217. All rights reserved.
//

import UIKit
import RealmSwift

class AddPhotoAlertController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var photo_desc: UITextField!
    @IBOutlet weak var choosenImage: UIImageView!
    
    let realm = try! Realm()
    
    var task = Task()
    var photo = Photo()
    var operation_type = 0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        cornerImage()
        
        if operation_type == 1{
            self.photo_desc.text = photo.desc
            self.choosenImage.image = UIImage(data: photo.picture!)
        }
    }
    
    func cornerImage(){
        self.choosenImage.layer.cornerRadius = self.choosenImage.frame.width/4.0
        self.choosenImage.clipsToBounds = true
    }
    
    @IBAction func cancelTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func saveTapped(_ sender: UIButton) {
        
        if operation_type == 0{
            createNewPhoto(desc: self.photo_desc.text, img: NSData(data: (self.choosenImage.image?.jpegData(compressionQuality: 0.9) ?? (self.choosenImage.image?.pngData()) ?? #imageLiteral(resourceName: "question").pngData()!)) as Data)
        }else{
            updatePhoto(desc: self.photo_desc.text, img: NSData(data: (self.choosenImage.image?.jpegData(compressionQuality: 0.9) ?? (self.choosenImage.image?.pngData()) ?? #imageLiteral(resourceName: "question").pngData()!)) as Data)
        }
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadPhotoData"), object: nil)
        
        self.dismiss(animated: true, completion: nil)
    }
    
    func updatePhoto(desc: String?, img: Data){
        let photo = realm.objects(Photo.self).filter("id == \(self.photo.id)").first
        try! realm.write {
            photo?.desc = desc ?? ""
            photo?.picture = img
        }
    }
    
    func createNewPhoto(desc: String?, img: Data){
        let task = realm.objects(Task.self).filter("id == \(self.task.id)").first
        try! realm.write {
            let newPhoto = Photo()
            newPhoto.id = IncrementaID_photo()
            newPhoto.desc = desc ?? ""
            newPhoto.picture = img
            task!.photos.append(newPhoto)
        }
    }
    
    @IBAction func chooseImageTapped(_ sender: UIButton) {
        let alert = UIAlertController(title: "Выберите изображение", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Камера", style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Галерея", style: .default, handler: { _ in
            self.openGallery()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Отмена", style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func openGallery()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary){
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.allowsEditing = true
            imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have permission to access gallery.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openCamera()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[.originalImage] as? UIImage {
            choosenImage.image = pickedImage
        }
        
        picker.dismiss(animated: true, completion: nil)
    }
}
