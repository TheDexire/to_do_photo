//
//  DetailController.swift
//  Realm_app
//
//  Created by user162217 on 10/1/20.
//  Copyright © 2020 user162217. All rights reserved.
//

import UIKit
import RealmSwift

class DetailController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    @IBOutlet weak var collectionView: UICollectionView!
    var task = Task()
    let realm = try! Realm()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = task.name
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.reloadView), name: NSNotification.Name(rawValue: "reloadPhotoData"), object: nil)
        setupLongPressGesture()
    }
    
    func setupLongPressGesture() {
            let longPressGesture:UILongPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(self.handleLongPress))
            longPressGesture.minimumPressDuration = 1.0 // 1 second press
            longPressGesture.delegate = self as? UIGestureRecognizerDelegate
            self.collectionView.addGestureRecognizer(longPressGesture)
    }
    
    @objc func handleLongPress(_ gestureRecognizer: UILongPressGestureRecognizer){
        if gestureRecognizer.state == .began {
            let touchPoint = gestureRecognizer.location(in: self.collectionView)
            if let indexPath = collectionView.indexPathForItem(at: touchPoint) {
                sheetAlertView(photo_id: task.photos[indexPath.row].id)
            }
        }
    }
    
    func sheetAlertView(photo_id: Int){
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Просмотр", style: .default , handler:{ (UIAlertAction)in
            let photo = self.realm.objects(Photo.self).filter("id == \(photo_id)").first
            
            self.performSegue(withIdentifier: "PhotoDetail_Segue", sender: photo)
        }))
        
        alert.addAction(UIAlertAction(title: "Редактировать", style: .default , handler:{ (UIAlertAction)in
            let photo = self.realm.objects(Photo.self).filter("id == \(photo_id)").first
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let myAlert = storyboard.instantiateViewController(withIdentifier: "alert") as! AddPhotoAlertController
            myAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            myAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
            myAlert.photo = photo!
            myAlert.operation_type = 1
            self.present(myAlert, animated: true, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Удалить", style: .destructive , handler:{ (UIAlertAction)in
            
            let photo = self.realm.objects(Photo.self).filter("id == \(photo_id)").first
            try! self.realm.write {
                self.realm.delete(photo!)
                self.reloadView()
            }
        }))
        
        alert.addAction(UIAlertAction(title: "Отмена", style: .cancel, handler:{ (UIAlertAction)in
            
        }))
        
        self.present(alert, animated: true, completion: {
            
        })
    }
    
    @objc func reloadView(){
        self.collectionView.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return task.photos.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "photo_cell", for: indexPath) as! PhotoCell
        
        cell.photo.image = UIImage(data: task.photos[indexPath.row].picture!)
        cell.desc.text = task.photos[indexPath.row].desc
        
        if task.photos[indexPath.row].status == 0{
            cell.check_view_active.backgroundColor = .none
        }else{
            cell.check_view_active.backgroundColor = .systemOrange
        }
        
        cell.layer.borderColor = UIColor.lightGray.cgColor
        cell.layer.borderWidth = 1
        cell.layer.cornerRadius = 8
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let cell = collectionView.cellForItem(at: indexPath) as! PhotoCell
        
        if task.photos[indexPath.row].status == 0{
            cell.check_view_active.backgroundColor = .systemOrange
            changeStatusImage(photo_id: task.photos[indexPath.row].id, photo_status: 1)
        }else{
            cell.check_view_active.backgroundColor = .none
            changeStatusImage(photo_id: task.photos[indexPath.row].id, photo_status: 0)
        }
    }
    
    
    @IBAction func addPhotoTapped(_ sender: UIBarButtonItem) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let myAlert = storyboard.instantiateViewController(withIdentifier: "alert") as! AddPhotoAlertController
        myAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        myAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        myAlert.task = self.task
        self.present(myAlert, animated: true, completion: nil)
    }
    
    func changeStatusImage(photo_id: Int, photo_status: Int){
        try! realm.write {
            //            self.task.photos[photo_id].status = photo_status
            let photo = self.realm.objects(Photo.self).filter("id == \(photo_id)").first
            photo?.status = photo_status
        }
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "PhotoDetail_Segue" {
            if let destinationVC = segue.destination as? DetailPhotoController {
                destinationVC.photo = sender as! Photo
            }
        }
    }
}
